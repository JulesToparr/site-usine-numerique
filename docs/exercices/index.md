# Exercices

## Étudiant(es) - Majeur Usine 4.0
### Usine Numérique : 
- [TP 1 : Maquette numérique](tp1.md)
- [TP 2 : Simulation physique ](tp2.md)
- [TP 3 : Jumeau numérique - Ascenseur](tp3.md)
- [TP 4 : Jumeau numérique - Robot (bonus)](tp6.md)

### Robotique : 
- [TP 1 : Universal robot - Initiation](tp.bvs.ur.1.md)
- [TP 2 : Universal robot - Mouvements avancées](tp.bvs.ur.2.md)
- [TP 3 : Fanuc - Initiation](tp.bvs.fanuc.1.md)
- [TP 4 : Fanuc - Palletisation](tp.bvs.fanuc.2.md)

--- 

## I4 PAUC FISE :
### Travaux dirigées:
- [TD 1 : Notion de simulation](td1.md)
- [TD 2 : Estimation du volume d'eau perdu](td2.md)
- [TD 3 : Optimisation d'un Processus industriel](td3.md)
- [TD 4 : Introduction au machine learning](td4.md)

## I4 & I5 PAUC FISE
### Travaux pratique :  
- [TP 1 : Maquette numérique - DMU](tp1.md)
- [TP 2 : Simulation physique ](tp2.md)
- [TP 3 : Cycle de vie des données - PLM](tp3.md)
- [TP 4 : Jumeau numérique Machine](tp4.md)
- [TP 5 : Conception de ligne](tp5.md)
- [TP 6 : Jumeau numérque d'un robot](tp6.md)
- [TP 7 : Jumeau numérique d'une ligne de production](tp7.md)

---
