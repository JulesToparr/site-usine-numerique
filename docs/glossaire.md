# Glossaire

| Notion | Définition |
| ------ | ---------- |
| Fabrication additive |  |
| Fablab | |
| AGV | |
| Cobot | |
| Exosquelette | |
| Mobilité | |
| IoT | |
| IIoT | |
| Smart product | |
| Réalité virtuelle  | |
| Jumeau numérique | |
| Réalité augmentée | |
| Cloud computing | |
| Big Data | |
| Data mining | |
| Cybersécurité | |
| SCADA | |
| MES | |
| MOM | |
| ERP | |
| CRM | |

