---
hide:
  - navigation
  - toc
---

# Simulation Bille - Rail

<script>
  var startpauseCb;
  var resetCb;

  function startpause(){
    startpauseCb();
  }

  function reset(){
    resetCb();
  }
</script>

<div id="sketch-holder-td2">
    <button class="md-button" onclick="startpause()">Start simulation</button>
    <button class="md-button" onclick="reset()">Reset simulation</button>

    
    <div class="slidecontainer">
        <span>Inclinason</span>
        <input type="range" min="-20" max="20" value="0" step="0.1" class="slider" id="myRange">
        <span id="angle"></span>
    </div>
</div>

<script>


  var slider = document.getElementById("myRange");
  var inclinaison = 0;

  slider.oninput = function() {
    var output = document.getElementById("angle");
    output.innerHTML = this.value;
    inclinaison = this.value;
  }

</script>


{% import 'theme/p5macro.html' as macro %}
{{macro.sketch("sketch1", "","

  var run = false;

  startpauseCb = function() {
    run = !run;
  }

  resetCb = function() {
    inclinaison = 0;
    document.getElementById('angle').innerHTML = inclinaison;
    document.getElementById('myRange').value = inclinaison;
    p.setup();
  }

    function mouse2coord(){
        let vec = p.createVector(p.mouseX, p.mouseY);

        let x = (vec.x - 50)/scale;
        let y = -(60+(vec.y - p.height)/scale);
    
        return p.createVector(x, y);
    }

    function rotateVector(rawvec, theta){
        let vec = rawvec.copy();
        vec.x = p.cos(theta) * rawvec.x - p.sin(theta) * rawvec.y;
        vec.y = p.sin(theta) * rawvec.x + p.cos(theta) * rawvec.y;
        return vec;
    }

    function sdfSegment(pos, a, b) {
        let pa = pos.copy().sub(a),
            ba = b.copy().sub(a);
        let h = p.constrain(p5.Vector.dot(pa, ba) / p5.Vector.dot(ba, ba), 0.0, 1.0);
        return pa.sub(ba.mult(h)).mag();
    }

    function sdfArc(pos, start_angle, end_angle, ra, rb) {
        let range = start_angle - end_angle;
        let midAngle = p.PI / 2.0 - start_angle + range / 2.0;
        let po = new p5.Vector(0,0);
       
        po = rotateVector(pos, midAngle);

        let aperture = range / 2.0; //(end_angle-start_angle)/2.0;
        let sc = new p5.Vector(p.sin(aperture), p.cos(aperture));
        po.x = p.abs(po.x);

        let result = (sc.y * po.x > sc.x * po.y
                ? po.copy().sub(sc.copy().mult(ra)).mag()
                : p.abs(po.mag() - ra)) - rb;   
        return result;
    }

    var scale = 0.5;

    var rail = {
        angle: 0,
        inclinaison : inclinaison,
        start : new p5.Vector(0,0),
        arc_start : new p5.Vector(0,0),
        intersection : new p5.Vector(0,0),
        arc_center: new p5.Vector(0,0),
        arc_end: new p5.Vector(0,0),
        end: new p5.Vector(0,0),
        radius : 0, //half radius
        arc_start_angle: 0,
        arc_end_angle :0
    }

    slider.oninput = function() {
        var output = document.getElementById('angle');
        output.innerHTML = this.value;
        
        inclinaison = this.value;
        
        createRail(rail.intersection.x, rail.intersection.y);
        ball.collide();
        ball.old_position = ball.position.copy().sub(ball.velocity.copy().mult(0.01));
    
        
    }

    function createRail(x, y){
        rail.intersection = p.createVector(x, y);

        let seg1 = 495;
        let seg2 = 880;
        let interDist = 405;
        let angle = 32.5*p.DEG_TO_RAD;
        let halfangle = (angle/2.0);
        let halfBisector = (p.PI - angle)/2;

        let distToIntersect = (interDist/2.0)/p.sin(halfBisector);
        rail.radius = p.tan(halfBisector)*distToIntersect;

        let relStart = p.createVector(-seg1-distToIntersect, 0);
        let relEnd = p.createVector(seg2+distToIntersect, 0);

        rail.inclinaison = inclinaison;

        relStart = rotateVector(relStart, rail.inclinaison*p.DEG_TO_RAD + halfangle);
        relEnd = rotateVector(relEnd, rail.inclinaison*p.DEG_TO_RAD - halfangle);

        rail.start = p5.Vector.add(rail.intersection,relStart);
        rail.end = p5.Vector.add(rail.intersection,relEnd);
        rail.angle = p5.Vector.angleBetween(
            p5.Vector.sub(rail.intersection, rail.start),
            p5.Vector.sub(rail.intersection, rail.end)
        );

        rail.arc_center = p5.Vector.sub(rail.intersection, rail.end).normalize().mult(-rail.radius / p.sin(rail.angle / 2.0));
        rail.arc_center.rotate(-rail.angle / 2.0);
        rail.arc_center.add(rail.intersection);
        let dir1 = p5.Vector.sub(rail.start, rail.intersection).normalize();
        let norm1 = p.createVector(dir1.y, -dir1.x).normalize();
        rail.arc_start = p5.Vector.add(
            rail.arc_center,
            norm1.copy().mult(rail.radius)
        );
        let dir2 = p5.Vector.sub(rail.intersection, rail.end).normalize();
        let norm2 = new p5.Vector(dir2.y, -dir2.x).normalize();
        rail.arc_end = p5.Vector.add(
            rail.arc_center,
            norm2.copy().mult(rail.radius)
        );
        let rel_arc_start = p5.Vector.sub(rail.arc_start, rail.arc_center);
        let rel_arc_end = p5.Vector.sub(rail.arc_end, rail.arc_center);
        rail.arc_start_angle = p.atan2(rel_arc_start.y, rel_arc_start.x);
        rail.arc_end_angle = p.atan2(rel_arc_end.y, rel_arc_end.x);

        rail.sdf = function(pos) {
            let seg1 = sdfSegment(pos, this.start, this.arc_start);
            let seg2 = sdfSegment(pos, this.arc_end, this.end);
            let relPos = pos.copy().sub(this.arc_center);
            let arc = sdfArc(
                relPos,
                this.arc_start_angle,
                this.arc_end_angle,
                this.radius,
                0.0
            );
            let sd = p.min(p.min(seg1, arc), seg2);
            return sd;
        }

        rail.grad_sdf = function(pos) {
            let x =
                this.sdf(p5.Vector.add(pos, new p5.Vector(0.5, 0))) -
                this.sdf(p5.Vector.sub(pos, new p5.Vector(0.5, 0)));
            let y =
                this.sdf(p5.Vector.add(pos, new p5.Vector(0, 0.5))) -
                this.sdf(p5.Vector.sub(pos, new p5.Vector(0, 0.5)));
            return p.createVector(x, y);
        }

        rail.draw = function() {
            

            p.push();
            p.translate(rail.intersection.x*scale, rail.intersection.y*scale);

            p.fill(80);
            p.rectMode(p.CENTER);
            p.rect(0,0,600*scale,320*scale, 10,10,0,0);

            p.fill(255); 
            //Draw angle ruler
            p.rotate(rail.inclinaison*p.DEG_TO_RAD);
            p.ellipse(0,0, 130,130);

            
            p.fill(180);
            p.ellipse(0,0, 60,60);
            p.fill(30);
            p.ellipse(0,0, 20,20);

            p.noFill(); 
            
            p.push();
            p.rotate(-p.HALF_PI);
            for(let i=0; i<360; i+=6){
                p.line(120/2, 0, 130/2, 0);
                p.rotate((36/5)*p.DEG_TO_RAD);
            }
            p.pop();

            p.push();
            p.rotate(-p.HALF_PI);
            for(let i=0; i<360; i+=36){
                p.line(100/2, 0, 130/2, 0);
                p.rotate(36*p.DEG_TO_RAD);
            }
            p.pop();

            p.pop();

            p.strokeWeight(3);
            p.line(this.start.x*scale, this.start.y*scale, this.arc_start.x*scale, this.arc_start.y*scale);
            p.line(this.arc_end.x*scale, this.arc_end.y*scale, this.end.x*scale, this.end.y*scale);

            p.noFill(); 
            p.arc(
                this.arc_center.x*scale,
                this.arc_center.y*scale,
                this.radius * 2.0*scale,
                this.radius * 2.0*scale,
                this.arc_end_angle,
                this.arc_start_angle
            );
            p.strokeWeight(6);
            p.point(this.intersection.x*scale, this.intersection.y*scale);
            p.point(this.start.x*scale, this.start.y*scale);
            p.point(this.end.x*scale, this.end.y*scale);
            p.point(this.arc_start.x*scale, this.arc_start.y*scale);
            p.point(this.arc_end.x*scale, this.arc_end.y*scale);
        }
    }

    
    var ball = {
        position: new p5.Vector(0,0),
        old_position: new p5.Vector(0,0),
        velocity: new p5.Vector(0,0),
        acceleration: new p5.Vector(0,0),
        radius: 10,
        mass : 0.1
    }

    function createBall(x, y) {

        ball.position = p.createVector(x,y);
        ball.old_position = p.createVector(x, y);
        ball.velocity = p.createVector(0, 0);
        ball.acceleration = p.createVector(0, 9.81);
        
        ball.collide = function() {
            //Rail collision
            let dist = rail.sdf(this.position) - this.radius;
            if (dist < 0) {
                let grad = rail.grad_sdf(this.position).mult(-1.0);
                this.position.add(grad.mult(dist));
                this.old_position.add(grad.mult(dist*0.5));
            }

            let ymax = (p.height-40)/scale;
            dist = ymax - ball.position.y - this.radius;
            if(dist < 0){
                this.position.y += dist;
                this.old_position.y -= dist*0.8;
            }

            let xmax = (p.width)/scale;
            dist = xmax - ball.position.x - this.radius;
            if(dist < 0){
                this.position.x += dist;
                this.old_position.x -= dist*0.8;
            }

            let xmin = 40/scale;
            dist = ball.position.x - xmin - this.radius;
            if(dist < 0){
                this.position.x -= dist;
                this.old_position.x += dist*0.8;
            }
            
        }

        ball.integrate = function(dt) {
            //verlet integration
            let deltaX = p5.Vector.sub(this.position, this.old_position);
            this.velocity = p5.Vector.div(deltaX, dt);
            this.old_position = this.position.copy();
            this.position = p5.Vector.add(this.position, deltaX.mult(0.999991)).add(
                this.acceleration.copy().mult(dt * dt)
            );
        }

        ball.draw = function() {
            p.fill(255);
            p.stroke(0);
            p.strokeWeight(1);
            p.ellipseMode(p5.CENTER);
            p.ellipse(
                this.position.x*scale,
                this.position.y*scale,
                this.radius * 1.9*scale,
                this.radius * 1.9*scale
            );
        }

    }


    function drawVRuler(){
        p.push();
        p.strokeWeight(1);
        if(p.mouseIsPressed){
            p.stroke(10);
            p.fill(255);
            p.rect(45, p.mouseY-35, 100, 20, 5, 5, 5, 5);
            p.fill(10);
            p.text('y:' , 60, p.mouseY-20);
            p.text(mouse2coord().y|0 , 80, p.mouseY-20);
            p.text('mm' , 110, p.mouseY-20);
        }

        p.noStroke();
        p.fill(p.color(52));
        p.rect(0,0,40,p.height);
        p.stroke(p.color(255));
        for(let i=p.height-40; i > 0; i-=10){
            p.line(0, i, 10, i);
        }
        for(let i=p.height-40; i > 0; i-=50){
            p.line(0, i, 25, i);
        }
        p.strokeWeight(2);
        p.line(30, p.mouseY-10, 40, p.mouseY-10);
        
        p.pop();
    }

    function drawHRuler(){
        p.push();

        p.strokeWeight(1);
        if(p.mouseIsPressed){
            p.stroke(10);
            p.fill(255);
            p.rect(p.mouseX-10, p.height-75, 100, 20, 5, 5, 5, 5);
            p.fill(10);
            p.text('x:' , p.mouseX, p.height-60);
            p.text(mouse2coord().x|0 , p.mouseX+20, p.height-60);
            p.text('mm' , p.mouseX+60, p.height-60);
        }

        p.fill(p.color(52));
        p.noStroke();
        p.rect(40,p.height-40,p.width,40);
        p.stroke(p.color(252));

        for(let i=40; i < p.width; i+=10){
            p.line(i, p.height, i, p.height-10);
        }
        for(let i=40; i < p.width; i+=50){
            p.line(i, p.height, i, p.height-25);
        }
        p.strokeWeight(2);
        p.line(p.mouseX-10, p.height-30, p.mouseX-10, p.height-40);
    
        p.pop();
    }

    let distance = 0;

    p.setup = function() {
        p.createCanvas(1050, 450).style('display', 'block');
        p.textFont('Poppins');
        p.textSize(16);
        //p.clear();
        //p.background(255, 0);

        createRail(450/scale, (330/scale)-110);
        createBall(rail.start.x + 10, rail.start.y - 10);
        ball.collide();
        ball.old_position = ball.position.copy();
        p.pixelDensity(1);
        ball.history = new Array();
        ball.history.push(ball.position);
        
    }



    p.draw = function() {
        p.background(255);

        if(run)
        for (let i = 0; i < 50; i++) {
            ball.collide();
            ball.integrate(0.01);
            ball.history.push(ball.position);

            if(ball.history.length>30000){
                ball.history = new Array();
                ball.history.push(ball.position);
                run = false;
            }
        }
        /*
        p.loadPixels();
        for (let y = 0; y < p.height; y++) {
            for (let x = 0; x < p.width; x++) {
                let s = p.cos(rail.sdf(new p5.Vector(x, y)));
                let grad = rail.grad_sdf(new p5.Vector(x, y));
                p.set(x, y, p.color(s*255, s*255, 100));
            }
        }
        p.updatePixels();
        */

        rail.draw();
        ball.draw();
        distance = 0;
        p.push()
        p.stroke(p.color(255,0,0));
        p.noFill();
        p.beginShape();
        for (let i = 0; i < ball.history.length; i++){
            p.curveVertex(ball.history[i].x*scale, ball.history[i].y*scale);
            if(i>0)distance+=(p5.Vector.dist( ball.history[i-1], ball.history[i]));
        }
        p.endShape();
        p.pop();

        p.push()
        p.strokeWeight(1);

        p.stroke(10);
        p.fill(255);
        p.rect(p.width-180, 30, 150, 50, 5, 5, 5, 5);
        p.fill(10);
        p.text('Distance parcourue:' , p.width-170, 45);
        p.text(distance|0 , p.width-170, 60);
        p.text('mm' , p.width-170+60, 60);
    
        p.pop();

        drawVRuler();
        drawHRuler();
    }

    var ballDrag = false;
    p.mousePressed = function(){
        if(p.mouseY <= 0 || p.mouseX <= 0 || p.mouseY >= p.height || p.mouseX >= p.width)return;

        let mp = p.createVector((p.mouseX-10)/scale, (p.mouseY-10)/scale);

        if(p5.Vector.dist(mp, ball.position) < ball.radius/scale){
            ballDrag = true;
            ball.position = ball.old_position = mp.copy();
        }
    }

    p.mouseReleased = function(){
        if(ballDrag){
            ballDrag = false;
            ball.collide();
            ball.collide();
            ball.collide();
            ball.old_position = ball.position.copy();
        }
    }

    p.mouseDragged = function(){
        if(p.mouseY <= 0 || p.mouseX <= 0 || p.mouseY >= p.height || p.mouseX >= p.width)return;

        let mp = p.createVector((p.mouseX-10)/scale, (p.mouseY-10)/scale);
        if(ballDrag){
            ball.position = ball.old_position = mp.copy();
        }
    }



")}}
