# Base de documentation

![](./images/library.png){.img-no-border align=left}

<a href="https://www.vecteezy.com/free-vector/school">School Vectors by Vecteezy</a>


<br>
Ici, vous trouverez toute la documentation technique en lien avec l'usine numérique.
Vous souhaitez contribuer à la documentation ? Suivez le guide !

[Contribuer](./contribute.md){.md-button .md-button--primary}

<br>
<br>

# Les guides disponible : 

=== "Siemens"

    ![Siemens](https://www.ville-rail-transports.com/wp-content/uploads/2018/10/siemens-logo.png){.img-small}

    [Siemens NX](./software/nx/nx.md){ .md-button .md-button--primary}
    [Siemens TIA](./software/tia/tia.md){ .md-button }
    [Siemens SIMIT](./software/simit/simit.md){ .md-button }
    [Siemens PLCSIM](./software/plcsim/plcsim.md){ .md-button }

=== "Dassault system"

    ![Dassault system](./images/dassault.png){.img-small}

    [3DExperience](./software/3dexperience/3dx.md){ .md-button}

=== "Schneider"

    ![Scnheider](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Schneider_Electric_2007.svg/2560px-Schneider_Electric_2007.svg.png){.img-small .img-grey-background}

    [Control Expert](./software/control-expert/control-expert.md){ .md-button}