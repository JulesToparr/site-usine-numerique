# Contribuer à la documentation

Vous soutaitez proposer une modification ? Ajouter de la documentation ? Vous êtes au bonne endroit !

Ce site est héberger sur [Gitlab.com](https://gitlab.com)
Nous utilisons [Gitlab page](https://docs.gitlab.com/ee/user/project/pages/) pour documenter et publier automatiquement grâce nottamment à [GitLab CI](https://about.gitlab.com/gitlab-ci/).

Lorsque vous éditez une pages et que les modifications sont envoyées , Gitlab regenère le site qui est alors accessible pour tout le monde.

Pour participer à la documentation, il vous suffit de cliquer sur l'icone [Crayon] en haut à droite de chaque page. Il vous faudra créer une compte gitlab

-> Commit
-> Merge request

-> Validation...