# Chapitre 1 : Contexte et définition

## 1. Une brève histoire de l'industrie

La transformation massive de l'industrie vers ce qui est appelé aujourd'hui l'industrie du futur ou industrie 4.0 est souvent définie comme la 4e révolution industrielle faisant suite à :

- la __mécanisation__
- la __production de masse__
- l'__automatisation__

![](./images/revIndus.png)

Il est important de prendre du recul vis-à-vis de ces transformations pour comprendre et réussir la transition en cours.
D'abord, ces révolutions ne sont pas aussi soudaines qu'il n'y parait. Chacune d'entre elles ont été initiée par les avancées lentes et minutieuses des sciences. Parfois accentuée par le contexte politique locale, elle reste principalement liée à l'histoire des sciences et techniques. Notons aussi que ces révolutions ne touchent pas tout les secteurs et tout les pays en même temps.

### 1.1. La mécanisation

La première révolution industrielle commence au __XVIII siècle__. Propulsé par l'exploitation du __minerai de charbon__ (La houille) permettant la production de vapeur d'eau, le XVIII siècle est alors le théâtre des plus importantes innovation en mécanique (machine à tisser, locomotive à vapeur...).

En commençant par la mécanisation de la filature puis du tissage en passant par les __machine à vapeur__, les manufactures isolées deviennent des usines mécanisé permettant de produire plus et plus vite. L'Homme est appuyé par la machine dans son labeur.
Cette première révolution industrielle se propage de la Grande-Bretagne à la France au début du XIXe siècle, avant de s'étendre à l'Allemagne, l'empire Russe ainsi qu'aux états-unis et au Japon.

![](./images/HistFactory0.jpg)

Les manufactures locales sont alors transformées en usine de plus grande envergure regroupant des centaines de travailleurs. Notamment, grâce aux voies de chemin de fer, les marchandises et les ouvriers peuvent être acheminé directement sur les sites de production.

### 1.2. Production de masse

Au début du XIXe siècle, le monde assistera à une deuxième révolution industrielle portée une fois de plus pas les innovations techniques et scientifiques.
Le XIXe marquera le début d'une course à l'innovation qui n'a cessé d'accélérer. Il sera le siècle des pionniers, et des grands inventeurs.
L'usage du gaz et du pétrole se généralise grâce à la mise au point du moteur à explosion (fin du XIXe).

Les innovations en métallurgie et en chimie de synthèse viennent elles aussi moderniser l'industrie.
Cette révolution est marquée par l'accroissement du rôle joué par la recherche et les capitaux. C'est l'avènement des grandes usines symbole du modèle d'organisation productive.

<figure class="image">
  <img src="./images/edisonlight.jpg" alt="Edison electric light instruction">
  <figcaption>Instruction pour allumer un ampoule à incandescence</figcaption>
</figure>

La propagation du réseau électrique donne plus de liberté pour rationaliser l'organisation spatiale des usines de façon strictement conforme aux étapes de fabrication du produit. C'est l'apparition du Taylorisme et des chaînes de montage.

<figure class="image">
  <img src="./images/HistFactory1.jpg" alt="Chaine de montage">
  <figcaption>Chaine de montage des voiture Ford (1908)</figcaption>
</figure>

Bien que les véhicules de la chaîne de productions Ford soit acheminé d'un poste de travail à l'autre grâce à un rail, la totalité des opérations de montage sont réalisé manuellement.

### 1.3. Production automatisée

Amorcée dès 1969, cette nouvelle étape dans l'histoire des sciences et des techniques est appuyée par l'utilisation d'une énergie nouvelle : l'Energie nucléaire. Mais ce n'est pas elle qui marquera profondément l'industrie.
Cette période sera profondément marquée par l'invention des circuits intégré puis du microprocesseur. En effet c'est le début de l'automatisation.
Le développement de l'électronique et de l'informatique rendent possible la production de composants miniaturisée de façon totalement automatisée.
Cette période verra naître les premiers robots industriels encore utilisé aujourd'hui.

![](./images/HistFactory2.jpg)

La production automatisée décrit un processus quasi-autonome de fonctionnement de l'usine. Pour autant, les opérateurs ont toujours un rôle majeur dans l'usine. Il contrôle, paramètre et accompagne les cellules de production.
Cependant les opérations de montage, peinture, assemblage, soudage ... Sont désormais assurée par des machines automatiques, soulageant ainsi les ouvriers, tout en améliorant la qualité des produits.

### 1.4. Industrie du futur

La 4ème révolution industrielle n'est pas seulement caractérisée par nos avancées technologiques. Elle est caractérisée par le fait que nos technologies évoluent désormais de manière exponentielle.
Il ne s'agit plus d'intégrer les nouvelles technologies à l'industrie, il s'agit d'intégrer l'innovation technologique dans la gestion de l'entreprise.

Toute la finesse de cette transition vers l'industrie du futur réside dans le fait qu'il faut distinguer parmi toutes les innovations de rupture apportée par le numérique, les révolutions plus profondes, ces révolutions que modifiera à long terme notre organisation de travail.
Il nous faudra également identifier les méthodes qui rendront notre industrie agile, adaptable aux innovations susceptible d'apparaître brutalement.

![](http://nomdezeus.fr/wp-content/uploads/2016/08/welcome-future-833x450.png)
