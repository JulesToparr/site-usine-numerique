# Chapitre 2 : Les enjeux de l'usine du futur

## L'usine numérique dans le monde

Contrairement aux précédentes révolutions industrielle, la quatrième touche presque le monde entier simultanément.
En France la transition vers le futur de l'industrie est propulsé par un projet nationnal appelé [Industrie du futur](https://www.economie.gouv.fr/lancement-seconde-phase-nouvelle-france-industrielle). Plus localement, dans les Hauts-de-france il s'agit du programme [Rev3](https://rev3.fr/).

Le projet industrie du futur est prévu pour s'interfacer avec le projet de modernisation bien plus important mené par l'allemagne souvent considérer comme un des leader de la modernisation de l'industrie.
En effet l'industrie représente 28% du PIB allemand, industrie qui a profité d'une relance économique après la deuxième guerre mondiale permettant de faire de l'allemagne un des piliers industriel européens. Toutefois, malgré ce qu'on entend souvent, l'allemagne n'est pas le plus gros industriel mondiale. Les entreprises américaines et chinoise en revanche apparaissent très régulièrement dans le haut du classements, tout comme, plus largement les entrerpise de l'union européene.

Comparativement, l'industrie en france ne représente que 12% du PIB national en effet, d'autre secteur comme le commerce, y occupent une place plus importante.
L'industrie française est veillissante et c'est d'ailleurs ce constats qui est à l'origine du programme national __industrie du futur__ qui a pour objectifs prioritaire de renouveller le parc industrielle et de se réaproprier nos moyen de production.

Même si le numérique fait partie intégrante de la vie du grand public, ce n'est que le début de son intégration dans l'industrie. Bien souvent, les responsable de site de prodution son conscient de la neccessité de moderniser les sytemes de production, pourtant ils manque souvent de méthode et de recul sur les technologie qui peuvent être utiliser pour gagner en __performance__, en __agilité__ et pour créer des usines __responsable__.

__A retenir__ : 

- L'allemagne à la pointe de l'industrie 4.0 en europe
- Les états unis concerve une certaine avance malgré un effort centré sur internet
- La chine envisage sérieusement de devenir le premier producteur mondiale
- La France accuse un léger retard comparativement à sa place de 7e puissance économique.

## Les enjeux de l'industrie de demain

Le principale défi que pose l'industrie du futur est basé sur une loi bien connu, la loi de Moore. Gordon Moore, ingénieur et homme d'affaire dans les années 70s à fait un constat encore valable aujourd'hui, le nombre de composant dans les circuits intégré double chaque année. Plus tard il corrigera sa loi avec une période de 2ans (x2 tout les 2 ans).
Depuis cette loi d'évolution n'a que très légèrement ralentit et nous continuons à miniaturiser les circuits imprimé. Rendant les ordinateurs de plus en plus puissant.
D'un manière générale on peut désormais dire que l'innovation technologique est devenu exponentiel. Ce n'est donc plus les solutions technologique qu'il faut intégrer à l'industrie mais l'innovation elle même afin de rester compétifif.

![Moore](https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Moore%27s_Law_Transistor_Count_1970-2020.png/1024px-Moore%27s_Law_Transistor_Count_1970-2020.png)

Heuresement plusieurs études existe afin d'identifier les technologie mûre des technologie balbutiante, sans quoi il peut parfois être difficile de s'orienter.
L'institut Gartner publie annuellement un graphique au nom évocateur : Le cycle du hype.
Ce graphe simple permet d'identifier très rapidement l'état de maturité d'une technologie. On remarque que bien souvent, on entend parler dans les médias d'une technologie puis elle tombe rapidement dans l'oublie avant de faire un retour plus timide une fois arrivier à maturité.

![Cycle du Hype](https://www.itforbusiness.fr/wp-content/uploads/2021/08/Gartner_hype_cycle_2021.png)

Notons bien que ce n'est pas parce qu'une technologie est populaire qu'il est pertinent de l'utiliser. En revanche il est important d'intégrer un programme de veille technologique afin d'identifier au plus tôt les technologie dont une entreprise peu tirer avantage.

![Climate](./images/climate.png)

__A retenir__ : 

- L'usine du futur c'est une usine : 
    - Performante -> Pour la compétitivité
    - Responsable -> Climat, respect des salariés
    - Agile -> Capable de s'adapter facilement 
- Les tendances suivent le cycle du hype (Gartner)
- L'innovation est exponentiel (Loi de moore)

## Nouveaux mode de consommation

Comme nous le disions en introduction, les modes de consommation évolue avec la société et l'industrie avec elles.
En effet, le consommateur est désormais plus exigeant qu'autrefois, d'abord un critère qui transforme massivement l'industrie, c'est la volonté de personnalisation.
Le client ne souhaite plus profiter du même produit que tout les autre (production de masse) mais preferera plutôt un bien personnalisé à son usage et à son goût.
Depuis quelques année ce changement est frappant sur les site des constructeur automobile et avec l'apparition des configurateur. Permettant de chsoisir son moteur, la couleur de la peinture, des sièges et même les modalité de réglement. Nous voulons du sur-mesure.

Ensuite le consommateur est désormais plus regardant sur la qualité des produit qu'il   consomme. Avec l'avènement des avis sur internet il est désormais plus facile de comparer les produit ou de faire savoir lorqu'un produit ne nous apporte pas satisfaction. La qualité déjà importante devient crucial pour les entreprise.

Enfin, avec l'avènement des achats en ligne, le suivi en temps réel de la production est désormais une option rassurante pour les clients.

__Pour résumé__ : 

- Suivi en temps réel
- Produit personnalisé
- Produits de meilleurs qualité


## Une usine performante
Par performance l'on imagine souvent des cadences de production très élevé. Pourtant ce n'es pas neccessairement le cas.
La performance industrielle vise à rechercher l'excellence opérationnelle. Pour l'atteindre, il faut réduire les coûts tout en augmentant la qualité des services et des produits.
Ainsi la performance n'est plus lié seulement au cadences de productions mais aussi à la qualité, la pertinence des produits vis à 

- Supprimez l'inutile
- Fluidifier les communications
- Préventions des pannes
- Optimisation de la supplychain (circuit court)
- Partage des données (Fournisseur <-> Entreprise)

## Une usine responsable
- Enjeux environnementaux
- Le bilan carbone (Taxe)
- Usine éthique

## Une usine agile
- Innovation continue
- Amélioration continue
- Réactivité
- L'usine modulaire (reconfigurable)

## Les axes de travail
- Produit connectée
- Usine connectée
- Maintenance prédictive
- Piloter les usines :
    - Suivi du parc machine
    - Gestion de la consommation
    - Optimisation et sécurisation
    - Allocation dynamique des ressources
    - Adpatation rapide de la production
    - Optimisation de la supply chain
    - Amélioration continue
    - Maitrise du risque

- Décloisonner les activités
- Aide à la décision
- Simuler l'usine
- Piloter l'usine
- Réduire la pénibilité
- Sécuriser les données et le process